"""
Script to produce plots for all DQ pages per fill and save them as pdf.
On an online machine, run:
python dq_plots_fill -f <fill> -d <ouput directory>
"""

import argparse
import subprocess
from math import floor
import yaml
import ROOT
from utils import get_rundb_info

#some cardcoded stuff
histoymlgit = "ssh://git@gitlab.cern.ch:7999/lhcb/histoyml.git"
histoymlpath = "OfflineDQ/Shift/"

parser = argparse.ArgumentParser(description="DQ plots per fill")
parser.add_argument('-r','--runs', dest='runs', required=True, type=str, help='Run range to plot (<start_run:end_run>)')
parser.add_argument('-d','--dir', dest='work_dir', required=False, type=str, default=".", help='directory to dump plots to')
args = parser.parse_args()
wd = args.work_dir.rstrip("/")
run_start_end = args.runs.split(":")

run_dir_from_run_no = lambda run_no: str(floor(run_no/10000)*10000)+"/"+str(floor(run_no/1000)*1000)

ROOT.gROOT.SetBatch(True)
# get zhe yammels
histoymlzip = subprocess.run(["git","archive","-o",f"{wd}/test.zip","--remote",histoymlgit,"main",histoymlpath], check=True, capture_output=True)
processNames = subprocess.run(["unzip","-u",f"{wd}/test.zip"],input=histoymlzip.stdout)

# loop zhe yammls
# ...
with open(f"{wd}/{histoymlpath}/CALO.yml", 'r') as plotyml:
  plotconf = yaml.safe_load(plotyml)
for run in range(int(run_start_end[0]),int(run_start_end[1])+1):
  run_info = get_rundb_info(run)
  if not (run_info["destination"] == "OFFLINE" and run_info["state"] == "MIGRATED"): continue
  print(run)
  # get file
  hconf = plotconf["histograms"][0]
  print(hconf)
  tn = hconf["taskname"]
  f = ROOT.TFile.Open(f"/hist/Savesets/ByRun/{tn}/{run_dir_from_run_no(run)}/{tn}-run{run}.root")
  h = f.Get(hconf["name"])
  c1 = ROOT.TCanvas()
  h.Draw()
  c1.SaveAs(f"{wd}/{hconf['name'].replace(' ','_')}.pdf")