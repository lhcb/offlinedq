"""
From an online machine, run like
  lb-dirac python -m dirac.dq_stats_weekly -c Beam6800GeV-VeloClosed-MagDown-Excl-UT
"""
import argparse
import DIRAC
DIRAC.initialize()
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
from utils import get_rundb_info
parser = argparse.ArgumentParser(description="""Offline DQ weekly statistics.
                                                Run like lb-dirac python -m dirac.dq_stats_weekly
                                                Prints list DQ flag, TCK, FullStat for a given stream, and run duration for runs (sorted by fills) for a given activity and condition""")
parser.add_argument('-f','--fill', dest='start_fill', required=False, type=int, default=0, help='First fill to print stats from (default: print all)')
parser.add_argument('-p','--partition', dest='partition', required=False, type=str, default='LHCb', help='Partition in which the data was taken (default: LHCb, a.k.a. the global partition)')
parser.add_argument('-r','--runtype', dest='run_type', required=False, type=str, default='Collision24', help='Run type under which the data was taken (default: Collision24)')
parser.add_argument('-c','--condition', dest='condition', required=False, type=str, default='Beam6800GeV-VeloClosed-MagDown', help='Condition description under which the data was taken (default: Beam6800GeV-VeloClosed-MagDown)')
args = parser.parse_args()

bk = BookkeepingClient()
config = {'ConfigName': args.partition, 'ConfigVersion': args.run_type, 'ConditionDescription': args.condition}
print(f"DQ statistics for configuration {config}\n")
print(f'Run    | DQ flag   | ExDQOK | TCK        | Lumi [1/nb] | Duration [h:mm:ss] | pileup | VELO opening | HLT1 rate [kHz]')
print('-'*118)
for fill in sorted((bk.getListOfFills(config))['Value']):
  if fill < args.start_fill: continue
  print(f'Fill {fill}')
  for run in sorted((bk.getRunsForFill(fill))['Value']):
    run_info = (bk.getRunInformations(int(run)))['Value']
    if run_info["DataTakingDescription"] == args.condition and run_info["Configuration Version"] == args.run_type:
      dq_flag = [b[1] for b in bk.getRunFilesDataQuality(int(run))['Value']][-1]
      extra_dq_flag = ''.join([edqf[0] for edqf in bk.getRunExtendedDQOK(int(run))['Value']])
      rundb_info = get_rundb_info(run)
      print(f'{run} | {dq_flag:9} | {extra_dq_flag:6} | {run_info["Tck"]:10} | {0.001*(rundb_info["endlumi"]-rundb_info["startlumi"]):11.2f} |            {run_info["RunEnd"]-run_info["RunStart"]} | {float(rundb_info["avMu"]):7.2f} | {float(rundb_info["veloOpening"]):11.5f} | {0.001*float(rundb_info["avHltPhysRate"]):15.1f}')
