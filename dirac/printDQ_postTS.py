"""
August 2024 - Irene Bachiller
Plot of the DQ per run in each fill
On an online machine, run:
lb-dirac python -m dirac.printDQ
"""

import argparse
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
from DIRAC.Interfaces.API.Dirac import Dirac
from utils import get_rundb_info

parser = argparse.ArgumentParser(description="""Prints list DQ flag""")
# post-TS 9804
# to add in November: 10209
parser.add_argument('-f','--fill', dest='start_fill', required=False, type=int, default=9804, help='First fill to print stats from (default: print all)')
parser.add_argument('-p','--partition', dest='partition', required=False, type=str, default='LHCb', help='Partition in which the data was taken (default: LHCb, a.k.a. the global partition)')
parser.add_argument('-r','--runtype', dest='run_type', required=False, type=str, default='Collision24', help='Run type under which the data was taken (default: Collision24)')
parser.add_argument('-c','--condition', dest='condition', required=False, type=str, default='Beam6800GeV-VeloClosed-MagUp', help='Condition description under which the data was taken (default: Beam6800GeV-VeloClosed-MagUp)')
#parser.add_argument('-c','--condition', dest='condition', required=False, type=str, default='Beam6800GeV', help='Condition description under which the data was taken (default: Beam6800GeV)')                                                                                    

args = parser.parse_args()

dirac = Dirac()
bk = BookkeepingClient()

#config = {'ConfigName': args.partition, 'ConfigVersion': args.run_type, 'ConditionDescription': args.condition}
#config = {'ConfigName': args.partition, 'ConfigVersion': args.run_type}
config = {'ConfigName': args.partition, 'ConfigVersion': args.run_type, 'ConfigType': args.run_type ,  'ConfigFill': args.start_fill}
          
for fill in sorted((bk.getListOfFills(config))['Value']):
  if fill < args.start_fill: continue

  for run in sorted((bk.getRunsForFill(fill))['Value']):
    run_info = (bk.getRunInformations(int(run)))['Value']
    dq_flag = [b[1] for b in bk.getRunFilesDataQuality(int(run))['Value']][-1]
    rundb_info = get_rundb_info(run)

    #    if run_info["DataTakingDescription"] == args.condition:
    print(f'{fill} | {run} | {dq_flag:9} | {0.001*(rundb_info["endlumi"]-rundb_info["startlumi"]):11.2f} | ') 


