import requests

def get_rundb_info(run):
  session = requests.Session()
  session.trust_env = False
  content = session.get(f"http://rundb-internal.lbdaq.cern.ch/api/run/{run}", verify = False)
  content.raise_for_status()
  return content.json()