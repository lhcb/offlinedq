Welcome to the offline data quality package!

This is a loose collection of scripts for OfflineDQ coordinators and shifters.

## Scripts and instructions for coordinators

Procedures like filling the spreadsheet and flagging runs manually are documented in the [instructions for coordinators](coordinator_instructions.md)