### Getting started
Clone this repository on an online machine and `cd` into it.

You need to be able to get a grid certificate. If you haven't done this from an online machine, create `~/.globus` and copy the `userkey.pem` and `usercert.pem` files from e.g. lxplus.

### Fill spreadsheet with available runs

1. Get a grid proxy (`lhcb-proxy-init`)
2. Check the spreadsheet for the last fill ()
3. Run `lb-dirac python -m dirac.dq_stats_weekly -f <last_fill_in_spreadsheet>`. The script takes further arguments, like conditions, run type, partition and stream. The condition is the most likely to change, e.g. use `-c Beam6800GeV-VeloClosed-MagDown-Excl-UT` to search for runs where the UT was excluded from global data-taking. Double check in logbook, rundb, or bookkeeping under which condition the data was taken. Run `lb-dirac python -m dirac.dq_stats_weekly -h` for more information.
4. The output from the script can be copy pasted directly to the spreadsheet. The delimiter `|` has to be set manually. You can adapt the formatting of previous runs with the paint roller symbol (highlight cells, click on the paint roller and then highlight the range you want to apply it to).

### Check whether flags in dirac and the spreadsheet match
Shifters fill in the spreadsheet manually, and set the flag in Monet. What matters for analysts is the flag in dirac. Since this procedure is error-prone, the flags should be cross-checked from time to time.
To do that, run the `dq_stats_weekly` script and compare DQ and `ExtendedDQOK` flags with the spreadsheet.

While it is possible to change flags from BAD to OK in Monet, the `ExtendedDQOK` flags make it impossible to go from OK to BAD or UNCHECKED.
Such runs should be flagged by manually, e.g.
```
lb-dirac dirac-bookkeeping-setdataquality-run 301937 BAD
```

To set the `ExtendedDQOK` flags manually, do e.g.
```
lb-dirac dirac-bookkeeping-setextendeddqok-run --Overwrite 306131 PLUME,UT
```
It has been useful to write a small bash loop to set the flags of many runs in parallel
```
for i in 306131 306137 306155; do lb-dirac dirac-bookkeeping-setextendeddqok-run --Overwrite $i PLUME,UT; done
```

### Issue board
In 2024, the pp data was divided in 4 concurrent Sprucing campaigns, and there is another division by blocks. The [issue board](https://gitlab.cern.ch/lhcb/offlinedq/-/boards/25990) is used to keep track of the flagging status of sprucing campaigns. The status can be either "Planned", "Production", "Flagging", "Consistency check" or "Done". The label description can be seen by hovering over the label in the board.

Once the Flagging status is "Done", the flagging statistics should be reported in the issue. As the data in 2024 is sub-divided in blocks, the respective statistics should also be reported in the issue.


### Plot DQ flags per lumi
First print the latest list of flags and lumis from online 
 `lb-dirac python -m dirac.printDQ_postTS > flags_postTS.tex`

Then run the script to plot - the tex file name is hardcoded in the script - ` lb-conda default python plotDQ_postTS.py`

Same with `plotDQ_cumulative_postTS.py` for the cumulative.

![PlotDQ](plotDQ.png)

