"""
August 2024 - Irene Bachiller
Plot of the DQ per run in each fill taken from the print of offlinedq/dirac/flags.tex in plus
Run:
 lb-conda default python plotDQ_postTS.py 
"""


import ROOT
import numpy as np

ROOT.gROOT.SetBatch(True)


c1 = ROOT.TCanvas()
c1.SetTopMargin(0.1)
c1.SetBottomMargin(0.15)
c1.SetLeftMargin(0.15)
c1.SetTicky()
c1.SetTickx()

hs  = ROOT.THStack()

h_ok = ROOT.TH1D("histo ok","OK flag",       300,9700,10250)
h_uc = ROOT.TH1D("histo unchecked","UC flag",300,9700,10250)
h_bad = ROOT.TH1D("histo bad","BAD flag"    ,300,9700,10250)

cgreen = ROOT.TColor.GetColor("#4daf4a");
cblue = ROOT.TColor.GetColor("#377eb8");
cred = ROOT.TColor.GetColor("#e41a1c");

f=open("flags_postTS_corrected_13Nov.tex","r")
lines=f.readlines()
fills=[]
runs=[]
flags=[]
lumis=[]

for x in lines:
  fills.append(x.split(' | ')[0])
  runs.append(x.split(' | ')[1])
  flags.append(x.split(' | ')[2])
  lumis.append(x.split(' |')[3])
f.close()


print('Unchecked runs post-TS: ') 
for i in range(0,len(runs)):

  run=runs[i]
  flag=flags[i]
  fill= float(fills[i])
  lumi=float(lumis[i])/1000. # from nb-1 to pb-1

  if (flag[0:2]=="OK"):
    h_ok.Fill(fill,lumi)
  if (flag[0:9]=="UNCHECKED"):
    h_uc.Fill(fill,lumi)
    if (fill!=9906 and fill!=9905 and fill!=9900 and fill!=9899 and fill!=9896): print(run)   
  if (flag[0:3]=="BAD"): h_bad.Fill(fill,lumi)



h_ok.SetFillColor(cgreen)
h_uc.SetFillColor(cblue)
h_bad.SetFillColor(cred)
h_ok.SetLineWidth(0)
h_uc.SetLineWidth(0)
h_bad.SetLineWidth(0)

hs.Add(h_ok)
hs.Add(h_bad)
hs.Add(h_uc)
hs.Draw("hist")


hs.SetMaximum(230.)
hs.GetXaxis().SetTitle("Fill")
hs.GetYaxis().SetTitle("Luminosity [pb^{#minus1}]")
hs.GetXaxis().SetTitleSize(0.055)
hs.GetXaxis().SetLabelSize(0.055)
hs.GetYaxis().SetTitleSize(0.055)
hs.GetYaxis().SetLabelSize(0.055)
hs.GetYaxis().SetTitleOffset(1.2)
hs.GetXaxis().SetTitleOffset(1.15)
hs.GetXaxis().SetLabelOffset(0.02)
hs.GetXaxis().SetNdivisions(505)

total_lumi = h_ok.Integral()+h_uc.Integral()+h_bad.Integral()
print('Total lumi: %.2f pb-1' %total_lumi)


bad_lumi = h_bad.Integral()
bad_per = 100.*bad_lumi/total_lumi

legend = ROOT.TLegend(0.2,0.55,0.45,0.75)
legend.SetFillStyle(0)
legend.SetTextSize(0.055)
legend.SetTextFont(132)
legend.AddEntry(h_ok ,"OK","f")
legend.AddEntry(h_uc,"Unchecked","f")
legend.AddEntry(h_bad ,"Bad ("+str(round(bad_per,1))+"%)","f")
legend.SetLineWidth(0)
legend.Draw("same")

txtLHCb =  ROOT.TPaveText(0.15,0.75,0.9,0.86,"NDC") 
txtLHCb.AddText("LHCb 2024 data quality")
txtLHCb.SetTextFont(132)
txtLHCb.SetTextSize(0.055) 
txtLHCb.SetTextAlign(11) 
txtLHCb.SetFillStyle(0) 
txtLHCb.SetBorderSize(0) 
txtLHCb.Draw("same") 

c1.RedrawAxis()
c1.Print("plots/plotDQ_postTS.pdf")
