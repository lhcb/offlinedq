"""
August 2024 - Irene Bachiller
Plot of the DQ per run in each fill taken from the print of offlinedq/dirac/flags.tex
Run:
 lb-conda default python plotDQ_cumulative_postTS.py 
"""


import ROOT
import numpy as np

ROOT.gROOT.SetBatch(True)


c1 = ROOT.TCanvas()
c1.SetTopMargin(0.1)
c1.SetBottomMargin(0.15)
c1.SetLeftMargin(0.15)

c1.SetTicky()
c1.SetTickx()

hs  = ROOT.THStack()
h_t_temp = ROOT.TH1D("histo total","total lumi",300,9800,10233)
h_ok_temp = ROOT.TH1D("histo ok","OK flag",300,9800,10233)
h_uf_temp = ROOT.TH1D("histo uf","Unflagged",300,9800,10233)
h_bad_temp = ROOT.TH1D("histo bad","BAD flag",300,9800,10233)

cgreen = ROOT.TColor.GetColor("#4daf4a");
cblue = ROOT.TColor.GetColor("#377eb8");
cred = ROOT.TColor.GetColor("#e41a1c");
cgray = ROOT.TColor.GetColor("#bdbdbd");

f=open("flags_postTS_corrected_28Oct.tex","r")
lines=f.readlines()
fills=[]
runs=[]
flags=[]
lumis=[]

for x in lines:
  fills.append(x.split(' | ')[0])
  runs.append(x.split(' | ')[1])
  flags.append(x.split(' | ')[2])
  lumis.append(x.split(' |')[3])
f.close()


lumigood =0
lumitotal=0

for i in range(0,len(runs)):
  run=runs[i]
  flag=flags[i]
  fill= float(fills[i])
  lumi=float(lumis[i])/1000000. # from nb-1 to fb-1


  h_t_temp.Fill(fill,lumi)
  
  if (flag[0:2]=="OK"):
    h_ok_temp.Fill(fill,lumi)

  if (flag[0:9]=="UNCHECKED"):
    h_uf_temp.Fill(fill,lumi)

  if (flag[0:3]=="BAD"):
    h_bad_temp.Fill(fill,lumi)


h_t = ROOT.TH1D("histo cumulative","total cumulative",300,9800,10233)
h_t = h_t_temp.GetCumulative()

h_ok = ROOT.TH1D("histo ok cumulative","ok cumulative",300,9800,10233)
h_ok = h_ok_temp.GetCumulative()

h_uf = ROOT.TH1D("histo uf cumulative","uf cumulative",300,9800,10233)
h_uf = h_uf_temp.GetCumulative()

h_bad = ROOT.TH1D("histo bad cumulative","bad cumulative",300,9800,10233)
h_bad = h_bad_temp.GetCumulative()


h_ok.SetFillColor(cgreen)
h_ok.SetLineWidth(0)

h_t.SetFillColor(cgray)
h_t.SetLineWidth(0)

h_bad.SetFillColor(cred)
h_bad.SetLineWidth(0)

h_uf.SetFillColor(cblue)
h_uf.SetLineWidth(0)

hs.Add(h_ok)
hs.Add(h_bad)
hs.Add(h_uf)
hs.Draw("hist")

hs.SetMaximum(8.)
hs.SetTitle("")
hs.GetXaxis().SetTitle("Fill")
hs.GetYaxis().SetTitle("Luminosity [fb^{#minus1}]")
hs.GetXaxis().SetTitleSize(0.055)
hs.GetXaxis().SetLabelSize(0.055)
hs.GetYaxis().SetTitleSize(0.055)
hs.GetYaxis().SetLabelSize(0.055)
hs.GetYaxis().SetTitleOffset(1.2)
hs.GetXaxis().SetTitleOffset(1.15)
hs.GetXaxis().SetLabelOffset(0.02)
hs.GetXaxis().SetNdivisions(505)

total_lumi = h_t_temp.Integral()
ok_lumi = h_ok_temp.Integral()
uf_lumi = h_uf_temp.Integral()
bad_lumi = h_bad_temp.Integral()
bad_per = 100*bad_lumi/total_lumi
print('Total lumi: %.2f pb-1' %total_lumi)
print('OK lumi: %.2f pb-1' %ok_lumi)
print('Uc lumi: %.2f pb-1' %uf_lumi)
print('Bad lumi: %.2f pb-1' %bad_lumi)

legend = ROOT.TLegend(0.2,0.5,0.45,0.75)
legend.SetFillStyle(0)
legend.SetTextSize(0.055)
legend.SetTextFont(132)

legend.AddEntry(h_ok ,"OK","f")
legend.AddEntry(h_uf ,"Unchecked","f")
legend.AddEntry(h_bad ,"Bad ("+str(round(bad_per,1))+"%)","f")

legend.SetLineWidth(0)
legend.Draw("same")

txtLHCb =  ROOT.TPaveText(0.15,0.75,0.9,0.86,"NDC") 
txtLHCb.AddText("LHCb 2024 data quality")
txtLHCb.SetTextFont(132)
txtLHCb.SetTextSize(0.055) 
txtLHCb.SetTextAlign(11) 
txtLHCb.SetFillStyle(0) 
txtLHCb.SetBorderSize(0) 
txtLHCb.Draw("same") 

c1.RedrawAxis()
c1.Print("plots/plotDQ_cumulative_postTS.pdf")
